from django.db import models

# Create your models here.
class Article(models.Model):
    headline = models.TextField()
    text = models.TextField()

    def __str__(self):
        return self.headline


class Comment(models.Model):
    text = models.TextField()
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return self.text